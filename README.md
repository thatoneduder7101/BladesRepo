## BLADES SKIN FOR KODI 20 (TetradUI Fork)
<p align="left">
<img src="https://gitlab.com/thatoneduder7101/BladesRepo/-/raw/5654a87aa4d753ff1f9241f01e448926b3c2c09f/screenshot-02.png" width="760" align="left">
</p>
<br>



##  INSTALL:
- Install from [latest zip](https://gitlab.com/thatoneduder7101/BladesRepo/-/releases) |
[Github Mirror](https://github.com/ThatOneDuder710/skin.Blades/releases)

- Requires:
script.embuary.helper v2.0.2  

<br>



## CREDITS: 
- Blades fork by ThatOneDuder710 |

- TeamUI:
[Gade](https://forum.kodi.tv/member.php?action=profile&uid=152411)
[Manfeed](https://forum.kodi.tv/member.php?action=profile&uid=81541)
[Ch3dd4rGoblin](https://forum.kodi.tv/member.php?action=profile&uid=465378)

- Original JX720 Skin by [Jezz_X](https://kodi.tv/addons/matrix/author/jezz_x)
<br>

## CHANGES
listed below are the main differences between Blades and TetradUI:
- Modified System Card to view CPU usage instead of IP address
- Modified clock/date position
- Changed Busy/loading icon 
- Button texture from JX720
- Background overlay from JX720

## LICENSE
This work is licensed under the Creative Commons Attribution-Share Alike 3.0 United States License.

To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/us/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
<br>
